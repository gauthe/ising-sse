This program computes an importance sampling of the 2nd neighbor Ising model in a transverse field on the triangular lattice using stochastic series expansion. Geometry is fixed at compilation time by defining variable SIZE, ROW and COL in the preprocessor.

The general hamiltonian of the transverse field Ising model (TFIM) is:
H = sum_{i,j} J_{i,j} sigma-i sigma-j - Gamma sum_i sigma_i^x


Using the SSE algorithm, adding some constants to the hamiltonian, we can compute Z from a "string" of hamiltonians H(a,b)
Z = sum beta^n/(n!) <alpha | H_{i(1),j(1)} x ... x H_{i(M),j(M)} | alpha>
with
H(0,0) = Id                          identity operator " ", ensure constant length of the string
H(1,k) = 3J/2 - J/2(sigma_i sigma_j + sigma_j sigma_k + sigma_k sigma_i)         plaquette k (defined in cluster.cpp), J = J_1, J_2
H(2,i+1) = Gamma                       spin "unflip" operator "U" at site i, allows to build clusters and to flip them
H(2,-i-1) = Gamma sigma-i^x            spin flip operator "F" at site i, flip site i


For an operator, convention for legs are:

-----> imaginary time ------->

4 -P- 5         site k
2 -P- 3         site j
0 -P- 1         site i

0 -U- 1         site i

0 -F* 1         site i
