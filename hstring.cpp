#include "hstring.hpp"
#include "cluster.hpp"
#include <time.h>


// constructor from physical parameters; use time(0) + P_diag to have different seeds when 2 jobs start at the same time (stay independant)
Hstring::Hstring (double beta, double gamma, double J2): n(0), M(std::size_t(beta*SIZE*(gamma+5))), P_diag(beta*SIZE*(gamma+2*2*(1+J2))), P_1st_nei(2*2/(gamma+2*2*(1+J2))), P_2nd_nei(2*2*(1+J2)/(gamma+2*2*(1+J2))), h_values(M,vertex(0,0)), clus(M,*this), gen(time(0)+int(P_diag))
{
#ifdef FORCE_STRIPE
  for (unsigned i=0;i<ROW;i++) { for (unsigned j=0;j<COL;j++) { alpha[i*COL+j] = (i+2*j)%2; } }   // force stripe phase at initialization
#else
  for (unsigned i=0; i<SIZE; ++i) { alpha[i] = gen()%2; }    // start with random alpha
#endif
}


// constructor from vertices (test & debug)
Hstring::Hstring (double beta, double gamma, double J2, state alpha_, std::vector<vertex> vertices): n(0), M(vertices.size()), P_diag(beta*SIZE*(gamma+2*2*(1+J2))), P_1st_nei(2*2/(gamma+2*2*(1+J2))), P_2nd_nei(2*2*(1+J2)/(gamma+2*2*(1+J2))), alpha(alpha_), h_values(vertices.size(),vertex(0,0)), clus(M,*this), gen(time(0)+int(P_diag))
{
  for (std::size_t p=0; p<M; ++p)
  {
    h_values[p] = vertices[p];
    if (h_values[p].first)
    {
      clus.fill_link(p,h_values[p]);
      n += 1;
    }
  clus.rejoin();
  }
}


// constructor from save data file
Hstring::Hstring (double beta, double gamma, double J2, std::size_t M_,FILE *fin): n(0), M(M_), P_diag(beta*SIZE*(gamma+2*2*(1+J2))), P_1st_nei(2*2/(gamma+2*2*(1+J2))), P_2nd_nei(2*2*(1+J2)/(gamma+2*2*(1+J2))), h_values(M), clus(M,*this)
{
  fread(&alpha,sizeof(state),1,fin);
  for(vertex& v:h_values) { fread(&v,sizeof(vertex),1,fin); }
  fread(&gen,sizeof(std::mt19937),1,fin);
  for (std::size_t p=0; p<M;p++) { n += h_values[p].first > 0; }
}


// destructor
Hstring::~Hstring()
{
}

// save current state in extern file to be able to relaunch computation
void Hstring::save (FILE *fout) const
{
  fwrite(&alpha,sizeof(state),1,fout);
  for (const vertex& v:h_values) { fwrite(&v,sizeof(vertex),1,fout); }
  fwrite(&gen,sizeof(std::mt19937),1,fout);
}



// adjust M to ensure n never reaches M
std::size_t Hstring::resize()
{
  if (M < 4*n/3)
  {
    M = 4*n/3;
    h_values.resize(M);
    clus.resize(M);
  }
  return M;
}


// print the string of hamiltonians
std::ostream & operator<< (std::ostream & out, Hstring const & hstr)
{
  out << "n = " << hstr.n << ", M = " << hstr.M;
  unsigned plaquette(0), spin_flip(0), spin_unflip(0);
  for (std::size_t p=0, M=hstr.M; p<M; ++p)
  {
    vertex v = hstr[p];
    if (v.first)
    {
      if (v.first == 1) { plaquette += 1; }
      else { v.second < 0 ? spin_flip += 1 : spin_unflip += 1; }
    }
  }
  out << std::endl << "plaquettes = " << plaquette << ", spin unflip = " << spin_unflip << ", spin flip = " << spin_flip << "\nalpha = "<< hstr.alpha;
  return out;
}



// remove a diagonal operator at position p using the Metropolis algorithm, return true if accepted
bool Hstring::rm_diag (std::size_t p)
{
  assert(h_values[p].first>0 && h_values[p].second>-1);              // check the operator can be removed

  if (dis(gen) < (M-n+1)/P_diag)      // if the update is accepted (P_diag = beta*SIZE*(gamma+2*2*(1+J2)))
  {
    h_values[p].first = 0;            // remove the operator; no need to change h_values[p].second because h_values[p].first only is checked to know whether there is a vertex
    n -= 1;
    return true;
  }
  return false;
}


// insert a diagonal operator at position p using the Metropolis algorithm, return true if accepted
bool Hstring::ins_diag (std::size_t p)
{
  assert(!h_values[p].first);                // check the operator can be inserted

  if (dis(gen) < P_diag/(M-n))               // if the update is accepted
  {
// choose which type of diagonal operator to insert, starting with highest probabity operator
    double prob = dis(gen);
    if ( prob < P_1st_nei ) { return ins_plaq(p,dis_plaq(gen)); }           // insert random 1st neighbour plaquette with given probability
    if ( prob < P_2nd_nei ) { return ins_plaq(p,dis_plaq(gen)+2*SIZE); }    // insert random 2nd neighbour plaquette
    return ins_unflip(p);     // insert spin unflip operator with given probability
  }
  return false;
}


// insert a plaquette at position p using the Metropolis algorithm, return true if accepted
bool Hstring::ins_plaq (std::size_t p, unsigned k)
{
  assert (!h_values[p].first);                  // check the operator can be inserted
// accept the insertion only if the plaquette is minimally frustrated
  if ((alpha[clus.plaq_list[k][0]]^alpha[clus.plaq_list[k][1]]) || (alpha[clus.plaq_list[k][1]]^alpha[clus.plaq_list[k][2]]))
  {
		h_values[p] = vertex(1,k);      // if i==k, j is minority site; insert the plaquette
		n += 1;
		return true;
  }
  return false;
}

// insert a spin unflip at position p using the Metropolis algorithm, return true if accepted
bool Hstring::ins_unflip (std::size_t p)
{
  assert(!h_values[p].first);                  // check the operator can be inserted
  h_values[p] = vertex(2,dis_site(gen));       // add the operator at random site i
  n += 1;
  return true;
}



// execute the cycle of diagonal updates
void Hstring::diag_updates ()
{
  clus.reset();                                                    // reset last[] to build new cluster linked list link[]
  clus.set_privileges(dis_conf(gen));                              // choose one of the 81 configuration for privileged sites
  for (std::size_t p=0; p<M; ++p)
  {
    if (!h_values[p].first) { ins_diag(p); }                       // if the operator is identity, try to insert a diagonal operator
    else if (h_values[p].second > -1) { rm_diag(p); }              // if the operator is a diagonal operator, try to remove it 
    else { alpha.flip(-h_values[p].second-1); }                    // else, propagate state after spin flip
    if (h_values[p].first)
    {
      clus.fill_link(6*p,h_values[p]);      // if there is an operator at position p after the move, update link[]
      if (h_values[p].first == 1)           // if there is a plaquette, update privileged == minority
      {
        assert (alpha[clus.plaq_list[h_values[p].second][0]]^alpha[clus.plaq_list[h_values[p].second][1]] || alpha[clus.plaq_list[h_values[p].second][1]]^alpha[clus.plaq_list[h_values[p].second][2]] || alpha[clus.plaq_list[h_values[p].second][2]]^alpha[clus.plaq_list[h_values[p].second][0]]);  // chech_values[p].second there is a minority site
       clus.min_pri[p] = clus.privileges[3*h_values[p].second/2/SIZE] == ( alpha[clus.plaq_list[h_values[p].second][0]] ^ alpha[clus.plaq_list[h_values[p].second][1]] ? alpha[clus.plaq_list[h_values[p].second][1]] ^ alpha[clus.plaq_list[h_values[p].second][2]] : 2 );
      }
    }
  }
  clus.rejoin();              // assure periodicity of clusters
}




// try to flip a cluster, ie turn its spin unflips into spin flips and vice-versa. Return true if accepted.
bool Hstring::flip_cluster (std::vector<std::size_t> const& vertices)
{
  if (!vertices.size() || dis(gen)<0.5) { return false; }          // if there is no operator to flip or flip is rejected, do not flip
  for(std::size_t i=0; i<vertices.size(); ++i)
  {
    h_values[vertices[i]].second *= -1;                 // turn spin unflips into spin flips and vice-versa
  }
  return true;
}


void Hstring::off_diag_updates ()
{
  state sites = state().set();              // i-bit becomes 0 after the firt cluster in which site i belongs has been visited. Allows to change state alpha if the cluster is flipped.

  std::vector<std::size_t> vertices;

  for (unsigned i=0; i<SIZE; ++i)                     // for every site
  {
    if (clus.last[i] < 0) { sites.flip(i); }          // if the spin never interacts, I cannot flip it. Mark it as processed.
  }

  for (unsigned i=0; i<SIZE; ++i)                     // for every site
  {
    if ( sites[i] && clus.link[clus.last[i]] > -1 )                  // if the cluster in which site i begins has not already been visited
    {
      sites.flip(i);                                                 // mark site i as processed
      vertices.resize(0);                                            // empty vector vertices
      clus.explore(clus.first[i], vertices);                   // explore the cluster in which i starts
      assert(clus.link[clus.first[i]] < 0);                          // check the cluster has been visited

      if (flip_cluster(vertices))                      // if the cluster has been flipped
      {
        alpha.flip(i);                                 // flip site i
        for (unsigned j=i+1; j<SIZE; ++j)              // for every other site j
        {
          if ( sites[j] && clus.link[clus.last[j]] < 0 )            // if site j has not been visited before and has now been visited, it begins in current cluster
          {
				    assert(clus.link[clus.first[j]] < 0);      // check the cluster has been visited
						sites.flip(j);                             // mark site j as processed
						alpha.flip(j);                             // flip site j
			  	}
        }
      }
      else                                             // if the cluster has not been flipped
      {
        for (unsigned j=i+1; j<SIZE; ++j)              // for every other site j
        {
          if ( sites[j] && clus.link[clus.last[j]] < 0 ) { sites.flip(j); }           // mark site j as processed if needed
        }
      }
    }
  }
  assert (sites == 0);   // check all sites have been processed

  // from now, all cluster containing the steps p=M-1 --> p=1 for at least one site have been visited, flipped with probability 1/2, and if flipped alpha has been updated. Now, I consider all the other clusters and try to flip them. No need to update alpha. Note that if a cluster contains no leg 0, it has no spin operator and extends in the whole imaginary time interval. Thus it contains a vertex found in last[] and has been found in the previous phase: I can only start exploration at leg 0 and find every clusters.
  for (std::size_t p=1; p<M; ++p)         // if there is a vertex at p=0, it is the first cluster for a site.
  {
    if (clus.link[6*p] > -1)              // if there is a cluster at site p and it has not been visited yet
    {
      vertices.resize(0);                 // empty vertices
      clus.explore(6*p, vertices);        // explore the cluster
      flip_cluster(vertices);             // try to flip it
    }
  }
  assert(clus.is_empty());                // check all clusters have been visited and link[] is now empty
}


void Hstring::sweep()
{
  diag_updates();
  off_diag_updates();
}


// start with random state = infinite temperature, gradually reduce temperature during thermalization
void Hstring::thermalize (unsigned q)
{
#ifndef FORCE_STRIPE
  unsigned q_therm = q/10;
  std::size_t M0(M);
  P_diag /= 10;
  M /= 10;
  h_values.resize(M);
  clus.resize(M);

  for (unsigned i=0; i<q_therm; ++i) { sweep(); }
  P_diag *= 2;
  if (M<M0/5) { M = M0/5; h_values.resize(M); clus.resize(M); }
  for (unsigned i=0; i<q_therm; ++i) { sweep(); }
  P_diag *= 2.5;
  if (M<M0/2) { M = M0/2; h_values.resize(M); clus.resize(M); }
  for (unsigned i=0; i<q_therm; ++i) { resize(); sweep(); }
  P_diag *= 2;
  if (M<M0) { M = M0; h_values.resize(M); clus.resize(M); }
#endif
  for (unsigned i=0; i<q; ++i) { resize(); sweep(); }
}
