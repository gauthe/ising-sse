#!/bin/sh
L=30

beta=6.00


for k in {5,8}; do
  J2=`echo "scale=2; $k/100" | bc | sed -e 's/^\./0./'`
  i=$(echo data_L"$L"beta"$beta"gamma*J2_"$J2".txt | wc -w)
  if [ $i -gt 1 ]; then
		echo $i";"$beta";"$J2";"$L > gamma-concatenate-$L-beta$beta-J2_$J2.txt
    for FILE in data_L"$L"beta"$beta"gamma*J2_"$J2".txt; do
		  sed -n 2p $FILE >> gamma-concatenate-$L-beta$beta-J2_$J2.txt     # parameters
		  sed -n 3p $FILE >> gamma-concatenate-$L-beta$beta-J2_$J2.txt     # observables
		  sed -n 5p $FILE >> gamma-concatenate-$L-beta$beta-J2_$J2.txt     # error estimation
    done
	fi
done

