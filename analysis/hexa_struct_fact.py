#!/usr/bin/python

from re import split
from sys import argv, exit
import numpy as np
from matplotlib import use
use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)    # LaTeX


# Get back parameters and data from file data_ising.txt.
with open(argv[1],'r') as data:
  geometry = split(";",data.readline())
  parameters = split(";",data.readline())
  observables = np.array(split(";", data.readline()),dtype=float)
  corrFunc = np.array(split(";",data.readline()),dtype=float)
  deltaObs = np.array(split(";", data.readline()),dtype=float)
  deltaCorr = np.array(split(";", data.readline()),dtype=float)
  
N = int(geometry[0])
row = int(geometry[1])
col = int(geometry[2])

if row != col:
  exit("row != col")


beta = float(parameters[0])
gamma = float(parameters[1])
J2 = float(parameters[2])
thermalization = int(parameters[3])
T_meas = int(parameters[4])
N_meas = int(parameters[5])

energy = observables[0]
m = observables[1]
FS = observables[2]
chi_m = observables[3]
capacity = observables[4]
stripe_order = observables[5]
stripe_chi = observables[6]
U = observables[7]

delta_energy = deltaObs[0]
delta_bond_order = deltaObs[1]
delta_FS = deltaObs[2]
delta_bond_chi = deltaObs[3]
delta_capacity = deltaObs[4]
delta_stripe_order = deltaObs[5]
delta_stripe_chi = deltaObs[6]
delta_U = deltaObs[7]


# lattice: triangular, with basis vectors a1 and a2. Reciprocal lattice is also triangular with basis vectors b1 and b2.
a1 = 1.0
a2 = np.exp(1j*np.pi/3)
b1 = 2*np.pi*(1-1j/np.sqrt(3))
b2 = 4*np.pi/np.sqrt(3)*1j

# we use complex as 2d-vectors. Define scalar product:
def dot(z1,z2):
  return z1.real*z2.real + z1.imag*z2.imag

# site j <=> R_j = vec(j)
def vec(j):
  return (j/col)*a2 + (j%col)*a1
I = np.array([i for i in range(row*col)])
J = vec(I)

# structure factor for vector k
def S(k):
  return sum(corrFunc*np.exp(-1j*dot(k,J)))

# error of the structur factor
def delta_S(k):
  return sum(deltaCorr*np.abs(np.cos(dot(k,J))))



##########################################################################################################################################################################################
# plot cuts between the middle of opposite sides

struc_fact_b1 = np.zeros(col+1,dtype=np.complex128)
delta_struct_b1 = np.zeros(col+1)
struc_fact_b2 = np.zeros(col+1,dtype=np.complex128)
delta_struct_b2 = np.zeros(col+1)
struc_fact_pi6 = np.zeros(col+1,dtype=np.complex128)
delta_struct_pi6 = np.zeros(col+1)
for i in range(col+1):
    struc_fact_b1[i] = S(i*b1/col)
    delta_struct_b1[i] = delta_S(i*b1/col)
    struc_fact_b2[i] = S(i*b2/row)
    delta_struct_b2[i] = delta_S(i*b2/row)
    struc_fact_pi6[i] = S(i*b1/col + i*b2/row)
    delta_struct_pi6[i] = delta_S(i*b1/col + i*b2/row)

#X = np.linspace(0,4*np.pi/np.sqrt(3),col+1)
X = np.linspace(0,4,col+1)

plt.figure()
plt.errorbar(X, struc_fact_b1.real, yerr=delta_struct_b1,marker="*",label=r"$\theta = -\pi/6$",color='g')
plt.errorbar(X, struc_fact_b2.real, yerr=delta_struct_b2,marker="*",label=r"$\theta = \pi/2$",color='b')
plt.errorbar(X, struc_fact_pi6.real, yerr=delta_struct_pi6,marker="*",label=r"$\theta = \pi/6$",color='r')

plt.xlim(-0.1,4+0.1)
plt.ylim(-N/10,11*N/10)
plt.title(r"$\beta = {},\; \Gamma = {},\; J_2 = {}$".format(beta,gamma,J2),fontsize=25)
plt.xlabel(r"k $\quad \times \pi/\sqrt{{3}}$",fontsize=22)
plt.ylabel(r"$\mathcal{S}(\textbf{k})$",fontsize=22)
plt.legend()
plt.savefig("cut-sides_hexaR{}C{}_beta{}gamma{}_J2_{}.png".format(row,col,beta,gamma,J2))

plt.clf()


##########################################################################################################################################################################################
# Now plot cuts from one vertex to the opposite one


struc_fact_0 = np.zeros(2*col/3+1,dtype=np.complex128)
delta_struct_0 = np.zeros(2*col/3+1)
struc_fact_pi3 = np.zeros(2*col/3+1,dtype=np.complex128)
delta_struct_pi3 = np.zeros(2*col/3+1)
struc_fact_2pi3 = np.zeros(2*col/3+1,dtype=np.complex128)
delta_struct_2pi3 = np.zeros(2*col/3+1)

for i in range(col/3+1):
  z = 2*i*b1/col + i*b2/row
  struc_fact_0[i] = S(z)
  delta_struct_0[i] = delta_S(z)
  struc_fact_0[2*col/3-i] = S(-z)
  delta_struct_0[2*col/3-i] = delta_S(-z)

  z = i*b1/col + 2*i*b2/row
  struc_fact_pi3[i] = S(z)
  delta_struct_pi3[i] = delta_S(z)
  struc_fact_pi3[2*col/3-i] = S(-z)
  delta_struct_pi3[2*col/3-i] = delta_S(-z)

  z = i*b1/col - i*b2/row
  struc_fact_2pi3[i] = S(z)
  delta_struct_2pi3[i] = delta_S(z)
  struc_fact_2pi3[2*col/3-i] = S(-z)
  delta_struct_2pi3[2*col/3-i] = delta_S(-z)

X_0 = np.linspace(0,8,2*col/3+1)
plt.figure()
plt.errorbar(X_0, struc_fact_0.real, yerr=delta_struct_0,marker="*",label=r"$\theta = 0$",color='b')
plt.errorbar(X_0, struc_fact_pi3.real, yerr=delta_struct_pi3,marker="*",label=r"$\theta = \pi/3$",color='g')
plt.errorbar(X_0, struc_fact_2pi3.real, yerr=delta_struct_2pi3,marker="*",label=r"$\theta = 2\pi/3$",color='r')

plt.xlim(-0.1, 8+0.1)
plt.ylim(-N/10,11*N/10)
plt.title(r"$\beta = {},\; \Gamma = {},\; J_2 = {}$".format(beta,gamma,J2),fontsize=25)
plt.xlabel(r"k $\quad \times \pi/3$",fontsize=22)
plt.ylabel(r"$\mathcal{S}(\textbf{k})$",fontsize=22)
plt.legend()
plt.savefig("cut-vertex_hexaR{}C{}_beta{}gamma{}_J2_{}.png".format(row,col,beta,gamma,J2))

