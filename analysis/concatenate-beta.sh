#!/bin/sh
L=30


for j in {1..19}; do
  gamma=`echo "scale=2; 5*$j/100" | bc | sed -e 's/^\./0./'`
  for k in {1..11}; do
    J2=`echo "scale=2; $k/100" | bc | sed -e 's/^\./0./'`
#    J2=0.00
    i=$(echo *gamma"$gamma"J2_"$J2".txt | wc -w)
    if [ $i -gt 1 ]; then
      echo $i";"$gamma";"$J2";"$L > beta-concatenate-$L-gamma$gamma-J2_$J2.txt
      for FILE in *gamma"$gamma"J2_"$J2".txt; do
        sed -n 2p $FILE >> beta-concatenate-$L-gamma$gamma-J2_$J2.txt     # parameters
        sed -n 3p $FILE >> beta-concatenate-$L-gamma$gamma-J2_$J2.txt     # observables
        sed -n 5p $FILE >> beta-concatenate-$L-gamma$gamma-J2_$J2.txt     # error estimation
      done
    fi
  done
done

