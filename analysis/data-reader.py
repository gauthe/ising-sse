#!/Users/arcos/Library/anaconda/bin/python

from sys import argv, exit
from re import split
import numpy as np

with open(argv[1],'r') as data:
  geometry = split(";",data.readline())
  parameters = split(";",data.readline())
  observables = np.array(split(";", data.readline()),dtype=float)
  corrFunc = np.array(split(";",data.readline()),dtype=float)
  deltaObs = np.array(split(";", data.readline()),dtype=float)
  deltaCorr = np.array(split(";", data.readline()),dtype=float)

N = int(geometry[0])
row = int(geometry[1])
col = int(geometry[2])

beta = float(parameters[0])
gamma = float(parameters[1])
J2 = float(parameters[2])
thermalization = int(parameters[3])
T_meas = int(parameters[4])
N_meas = int(parameters[5])

energy = observables[0]
m = observables[1]
FS_flip = observables[2]
chi_m = observables[3]
capacity = observables[4]
stripe_order = observables[5]
stripe_chi = observables[6]
U = observables[7]
FS_nei2 = observables[8]

delta_energy = deltaObs[0]
delta_m = deltaObs[1]
delta_FS_flip = deltaObs[2]
delta_chi_m = deltaObs[3]
delta_capacity = deltaObs[4]
delta_stripe_order = deltaObs[5]
delta_stripe_chi = deltaObs[6]
delta_U = deltaObs[7]
delta_FS_nei2 = deltaObs[8]

print "Geometry: HEXA, SIZE = {}, ROW = {}, COL = {}".format(N,row,col)
print "Physical parameters: beta = {}, gamma = {}, J2 = {}".format(beta,gamma,J2)
print "Simulation parameters: thermalization = {}, T_meas = {}, N_meas = {}".format(thermalization, T_meas,N_meas)

print "energy = {} +/- {}".format(energy,delta_energy)
print "m = {} +/- {}".format(m,delta_m)
print "FS_flip = {} +/- {}".format(FS_flip,delta_FS_flip)
print "chi_m = {} +/- {}".format(chi_m,delta_chi_m)
print "capacity = {} +/- {}".format(capacity,delta_capacity)
print "stripe_order = {} +/- {}".format(stripe_order,delta_stripe_order)
print "stripe_chi = {} +/- {}".format(stripe_chi,delta_stripe_chi)
print "U = {} +/- {}".format(U,delta_U)
print "FS_nei2 = {} +/- {}".format(FS_nei2,delta_FS_nei2)
