#!/usr/bin/python

from sys import argv, exit
from re import split
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import use
use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)    # LaTeX



# Get back parameters and data from file data_ising.txt.
with open(argv[1]) as data:
  geometry = split(";",data.readline())
  parameters = split(";",data.readline())
  observables = np.array(split(";", data.readline()),dtype=float)
  corrFunc = np.array(split(";",data.readline()),dtype=float)
  errorObs = np.array(split(";", data.readline()),dtype=float)
  deltaCorr = np.array(split(";", data.readline()),dtype=float)


N = int(geometry[0])
row = int(geometry[1])
col = int(geometry[2])

beta = float(parameters[0])
gamma = float(parameters[1])
J2 = float(parameters[2])
thermalization = int(parameters[3])
T_meas = int(parameters[4])
N_meas = int(parameters[5])
"""
# old data without J2:
J2 = 0
thermalization = int(parameters[2])
T_meas = int(parameters[4])
N_meas = int(parameters[3])
"""

# lattice: square, with basis vectors a1 and a2. Reciprocal lattice is also square with basis vectors b1 and b2.
a1 = 1.0
a2 = 1j
b1 = 2*np.pi
b2 = 2*np.pi*1j


# we use complex as 2d-vectors. Define scalar product:
def dot(z1,z2):
  return z1.real*z2.real + z1.imag*z2.imag

# site j <=> R_j = vec(j)
def vec(j):
  return (j/col)*a2 + (j%col)*a1
I = np.array([i for i in range(row*col)])
J = vec(I)

# structure factor for vector k
def S(k):
  return sum(corrFunc*np.exp(-1j*dot(k,J)))

# error of the structur factor
def delta_S(k):
  return sum(deltaCorr*np.abs(np.cos(dot(k,J))))

# compute structure factor
kx = np.linspace(0,(col-1.)/col,col)
ky = np.linspace(0,(row-1.)/row,row)
struc_fact = np.zeros((row,col),dtype=np.complex128)    # imaginary part should be 0
delta_struct = np.zeros((row,col))
for i in range(row):
  for j in range(col):
    struc_fact[i,j] = S(kx[i]*b1+ky[j]*b2)
    delta_struct[i,j] = delta_S(kx[i]+ky[j])

# plot structure factor
A,B = np.meshgrid((kx*b1).real,(ky*b2).imag)
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlabel(r"$k_x$")
ax.set_xlim(0, 2*np.pi)
ax.set_ylabel(r"$k_y$")
ax.set_ylim(0, 2*np.pi)
ax.set_zlabel(r"$\mathcal{S}(\textbf{k})$")
ax.set_zlim(0,N)
ax.set_title(r"$\beta = {},\; \Gamma = {},\; J_2 = {}$".format(beta,gamma,J2),fontsize=20)
surf = ax.plot_surface(A, B, struc_fact.real.T, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%1.0f'))    # round scale annotations to 1
#fig.colorbar(surf, shrink=0.5, aspect=5)      # add colorbar on the right side

plt.savefig("SF_squareR{}C{}_beta{}gamma{}.png".format(row,col,beta,gamma))

if row != col:
  exit("row != col")

plt.clf()
X_0 = np.linspace(0,2*np.pi*(col-1)/col,col)
plt.errorbar(X_0, struc_fact[row/2,:].real, yerr=delta_struct[row/2,:],marker="*",label=r"$\theta = 0$",color='b')
plt.errorbar(X_0, struc_fact[:,col/2].real, yerr=delta_struct[:,col/2],marker="*",label=r"$\theta = \pi/2$",color='g')

plt.xlim(0,2*np.pi*(col-1)/col)
plt.ylim(-N/10,11*N/10)
plt.title(r"$\beta = {},\; \Gamma = {},\; J_2 = {}$".format(beta,gamma,J2))
plt.xlabel(r"$||\textbf{k}||$")
plt.ylabel(r"$\mathcal{S}(\textbf{k})$")
plt.legend()
plt.savefig("cut-sides_squareR{}C{}_beta{}gamma{}_J2_{}.png".format(row,col,beta,gamma,J2))



plt.clf()
X_diag = np.linspace(0,np.sqrt(2)*2*np.pi*(col-1)/col,col)
Ypi4 = np.zeros(col)
Y3pi4 = np.zeros(col)
delta_pi4 = np.zeros(col)
delta_3pi4 = np.zeros(col)
for i in range(1,col):
  Ypi4[i] = struc_fact[i,i].real
  Y3pi4[i] = struc_fact[i,col-i].real
  delta_pi4[i] = delta_struct[i,i]
  delta_3pi4[i] = delta_struct[i,col-i]
Ypi4[0] = struc_fact[0,0].real
Y3pi4[0] = struc_fact[0,0].real
delta_pi4[0] = delta_struct[0,0]
delta_3pi4[0] = delta_struct[0,0]

plt.errorbar(X_diag, Ypi4, yerr=delta_pi4,marker="*",label=r"$\theta = \pi/4$",color='b')
plt.errorbar(X_diag, Y3pi4, yerr=delta_3pi4,marker="*",label=r"$\theta = 3\pi/4$",color='g')

plt.xlim(0, np.sqrt(2)*2*np.pi*(col-1)/col)
plt.ylim(-N/10,11*N/10)
plt.title(r"$\beta = {},\; \Gamma = {},\; J_2 = {}$".format(beta,gamma,J2))
plt.xlabel(r"$||\textbf{k}||$")
plt.ylabel(r"$\mathcal{S}(\textbf{k})$")
plt.legend()
plt.savefig("cut-vertex_squareR{}C{}_beta{}gamma{}_J2_{}.png".format(row,col,beta,gamma,J2))
