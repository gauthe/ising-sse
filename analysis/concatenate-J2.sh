#!/bin/sh
L=12

beta=6.00


for k in {1..38}; do
  gamma=`echo "scale=2; 5*$k/100" | bc | sed -e 's/^\./0./'`
  i=$(echo data_L"$L"beta"$beta"gamma"$gamma"J2_* | wc -w)
  if [ $i -gt 1 ]; then
		echo $i";"$beta";"$gamma";"$L > J2-concatenate-$L-beta$beta-gamma$gamma.txt
    for FILE in data_L"$L"beta"$beta"gamma"$gamma"J2_*; do
		  sed -n 2p $FILE >> J2-concatenate-$L-beta$beta-gamma$gamma.txt     # parameters
		  sed -n 3p $FILE >> J2-concatenate-$L-beta$beta-gamma$gamma.txt     # observables
		  sed -n 5p $FILE >> J2-concatenate-$L-beta$beta-gamma$gamma.txt     # error estimation
	  done
	fi
done

