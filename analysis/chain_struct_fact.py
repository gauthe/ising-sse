#!/usr/bin/python

from sys import argv
from re import split
import numpy as np
from matplotlib import use
use('Agg')
import matplotlib.pyplot as plt



# Get back parameters and data from file data_ising.txt.
with open(argv[1]) as data:
  geometry = split(";",data.readline())
  parameters = split(";",data.readline())
  dataObservables = split(";", data.readline())
  dataCorrFunc = split(";",data.readline())

N = int(geometry[0])

beta = float(parameters[0])
gamma = float(parameters[1])
J2 = float(parameters[2])
thermalization = int(parameters[3])
T_meas = int(parameters[4])
N_meas = int(parameters[5])

energy = float(dataObservables[0])
St_mag = float(dataObservables[1])

corr_func = [0]*N
I = np.linspace(0,N-1,N)
for i in range(N):
  corr_func[i] = float(dataCorrFunc[i])

# compute structure factor
k = 2*np.pi*I/N
S = np.zeros(N)
S_im = np.zeros(N)          # should be 0
for j in range(N):
  z = sum(np.exp(-1j*k[j]*I)*corr_func)
  S[j] = z.real
  S_im[j] = z.imag

# plot structure factor
plt.plot(k,S, label = "max(S) = {}\nphi = {}".format(round(max(S),2),St_mag))
plt.xlim(0, 2*np.pi)
#plt.ylim(0,25)
plt.xlabel("k")
plt.ylabel("S(k)")
plt.legend()
plt.title("N = {}, beta = {}, gamma = {}, N_meas = {}".format(N,beta,gamma,N_meas))
plt.savefig("struc_fact_N{}beta{}gamma{}.png".format(N,beta,gamma))

