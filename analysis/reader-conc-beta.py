#!/usr/bin/python

from sys import argv, exit
from re import split
import numpy as np
from matplotlib import use
use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)  # LaTeX


with open(argv[1],'r') as data_file:
  dataRaw = data_file.readlines()

param = split(";",dataRaw[0])
n_beta = int(param[0])
gamma = float(param[1])
J2 = float(param[2])
L = int(param[3])

beta = np.zeros(n_beta)
energy, m, FS_flip, chi_m, capacity, stripe_order, chi_stripe, U, FS_nei2 = [np.zeros(n_beta) for i in range(9)]
delta_energy, delta_m, delta_FS_flip, delta_chi_m, delta_capacity, delta_stripe_order, delta_chi_stripe, delta_U, delta_FS_nei2 = [np.zeros(n_beta) for i in range(9)]

for i in range(n_beta):
  data = split(";",dataRaw[3*i+1])
  beta[i] = float(data[0])
  if abs(gamma - float(data[1])) + abs(J2 - float(data[2])) > 0.000001:
    exit("params are not constant")
  data = split(";",dataRaw[3*i+2])
  energy[i] = float(data[0])
  m[i] = float(data[1])
  FS_flip[i] = float(data[2])
  chi_m[i] = float(data[3])
  capacity[i] = float(data[4])
  stripe_order[i] = float(data[5])
  chi_stripe[i] = float(data[6])
  U[i] = float(data[7])
  FS_nei2[i] = float(data[8])
  data = split(";",dataRaw[3*i+3])
  delta_energy[i] = float(data[0])
  delta_m[i] = float(data[1])
  delta_FS_flip[i] = float(data[2])
  delta_chi_m[i] = float(data[3])
  delta_capacity[i] = float(data[4])
  delta_stripe_order[i] = float(data[5])
  delta_chi_stripe[i] = float(data[6])
  delta_U[i] = float(data[7])
  delta_FS_nei2[i] = float(data[8])


# plot energy
plt.figure()
plt.errorbar(beta, energy, yerr=delta_energy ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='b')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta",fontsize=20)
plt.ylabel("energy",fontsize=20)
plt.legend()
plt.savefig("energy-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()


# plot order parameter m
plt.figure()
plt.errorbar(beta, m, yerr=delta_m ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='r')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta",fontsize=20)
plt.ylim(0,0.9)
plt.ylabel("m (order parameter)",fontsize=20)
plt.legend()
plt.savefig("m-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()


# plot FS flip
plt.figure()
plt.errorbar(beta, FS_flip, yerr=delta_FS_flip ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='g')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta", fontsize=20)
plt.ylabel("FS flip")
plt.legend()
plt.savefig("FS_flip-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()

# plot m susceptiblity
plt.figure()
plt.errorbar(beta, chi_m, yerr=delta_chi_m ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='b')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta",fontsize=20)
plt.ylabel(r"$\chi$ \text{{(order parameter m)}}")
plt.legend()
plt.savefig("chi_m-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()

# plot stripe order parameter
plt.figure()
plt.errorbar(beta, stripe_order, yerr=delta_stripe_order ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='r')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta",fontsize=20)
plt.ylim(0,1)
plt.ylabel("stripe order parameter",fontsize=18)
plt.legend(loc=2)
plt.savefig("stripe_order-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()

# plot stripe order susceptibility
plt.figure()
plt.errorbar(beta, chi_stripe, yerr=delta_chi_stripe ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='b')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta",fontsize=20)
plt.ylabel(r"$\chi$ (stripe order parameter)")
plt.legend()
plt.savefig("chi_stripe_order-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()

# plot thermal capacity
plt.figure()
plt.errorbar(beta, capacity, yerr=delta_capacity ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='b')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta",fontsize=20)
plt.ylabel("capacity")
plt.legend()
plt.savefig("capacity-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()


# plot binder cumulant
plt.figure()
plt.errorbar(beta, U, yerr=delta_U ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='g')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta",fontsize=20)
plt.ylabel("U")
plt.ylim(0,1)
plt.legend()
plt.savefig("U-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()


# plot FS nei2
plt.figure()
plt.errorbar(beta, FS_nei2, yerr=delta_FS_nei2 ,marker="+",linestyle='-',fillstyle='none',label="L = {}".format(L),color='r')
plt.title(r"\Gamma = {}, J_2 = {}".format(gamma,J2),fontsize=20)
plt.xlabel(r"\beta",fontsize=20)
plt.ylabel("FS 2nd neighbour")
plt.legend()
plt.savefig("FS_nei2-L{}-gamma{}-J2{}.png".format(L,gamma,J2))
plt.clf()
