#!/usr/bin/python

# merge two sets of data from two independant Monte Carlo simulations with same physical parameters. 
# formula: Delta3 = sqrt( N1*(N1*Delta1**2 + E1**2) + N2*(N2*Delta2**2 + E2**2) - (N1 + N2)*mean(E1,E2)**2 ) / (N1 + N2)

from sys import argv, exit
from re import split
import numpy as np


# Get back parameters and data in file 1
with open(argv[1]) as data1:
  geometry1 = split(";",data1.readline())
  parameters1 = split(";",data1.readline())
  observables1 = np.array(split(";", data1.readline()),dtype=float)
  corrFunc1 = np.array(split(";",data1.readline()),dtype=float)
  errorObs1 = np.array(split(";", data1.readline()),dtype=float)
  errorCorr1 = np.array(split(";", data1.readline()),dtype=float)

# Get back parameters and data in file 2
with open(argv[2]) as data2:
  geometry2 = split(";",data2.readline())
  parameters2 = split(";",data2.readline())
  observables2 = np.array(split(";", data2.readline()),dtype=float)
  corrFunc2 = np.array(split(";",data2.readline()),dtype=float)
  errorObs2 = np.array(split(";", data2.readline()),dtype=float)
  errorCorr2 = np.array(split(";", data2.readline()),dtype=float)

# check simulation parameters are the same
if geometry1 != geometry2:
  exit("geometry is not the same")
if parameters1[0] != parameters2[0]:
  exit("beta is not the same")
if parameters1[1] != parameters2[1]:
  exit("gamma is not the same")
if parameters1[2] != parameters2[2]:
  exit("J2 is not the same")

# store data in numbers
N = int(geometry1[0])
row = int(geometry1[1])
col = int(geometry1[2])
beta = float(parameters1[0])
gamma = float(parameters1[1])
J2 = float(parameters1[2])

N1 = int(parameters1[5])
N2 = int(parameters2[5])


# use optimized versions of the first formula
if N1 == N2:
  def mean(E1,E2):
    return 0.5*(E1 + E2)
  def mean_err(E1, Delta1, E2, Delta2):
    return 0.5*np.sqrt( Delta1**2 + Delta2**2 + (E1 - E2)**2/2/N1 )
else:
  def mean(E1,E2):
    return (N1*E1 + N2*E2)/(N1 + N2)
  def mean_err(E1, Delta1, E2, Delta2):
    return np.sqrt( (N1*Delta1)**2 + (N2*Delta2)**2 + N1*N2*(E1 - E2)**2/(N1 + N2) ) / (N1 + N2)

# compute mean values and error bars of the mean value
observables3 = mean(observables1,observables2)
errorObs3 = mean_err(observables1, errorObs1, observables2, errorObs2)
corrFunc3 = mean(corrFunc1, corrFunc2)
errorCorr3 = mean_err(corrFunc1, errorCorr1, corrFunc2, errorCorr2)

# store mean values in new file
with open("mean-data_"+split("data_", argv[1])[1],"w") as data_output:
  data_output.write(";".join(geometry1))
  parameters1[5] = "{}\n".format(N1+N2)     # data now contains N1+N2 measurements
  data_output.write(";".join(parameters1))
  data_output.write( ";".join(list(np.array(observables3,dtype=str)))+"\n")
  data_output.write( ";".join(list(np.array(corrFunc3,dtype=str)))+"\n")
  data_output.write( ";".join(list(np.array(errorObs3,dtype=str)))+"\n")
  data_output.write( ";".join(list(np.array(errorCorr3,dtype=str)))+"\n")

