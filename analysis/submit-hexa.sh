L=30
ISING_DIR=/home/gauthe/ising
EXE=$ISING_DIR/build-$L/ising-$L

for i in {2..20}; do
  beta=$(bc <<< "scale=2; $i*20/100")
  gamma=$(bc <<< "scale=1; 3/10")
  for j in {0..2}; do
    J2=$(bc <<< "scale=2; $j*10/100")
    INPUT=$ISING_DIR/input/long-paramsBeta"$beta"gamma"$gamma"_J2_"$J2".in
    cp $ISING_DIR/input/input-long.txt $INPUT
    sed -i "1s/.*/beta = "$beta"/" $INPUT
    sed -i "2s/.*/gamma = "$gamma"/" $INPUT
    sed -i "3s/.*/J2 = "$J2"/" $INPUT

    LOGS=$ISING_DIR/data-$L-hexa/logs-"$L"-"$beta"-"$gamma"-"$J2"
    RUN=$ISING_DIR/run/"$L"-"$beta"-"$gamma"-"$J2".run

    cp $ISING_DIR/run/run-hexa.run $RUN
    sed -i "2s@.*@#SBATCH --workdir /home/gauthe/ising/data-"$L"-hexa@" $RUN
    sed -i "11s@.*@"$EXE" "$INPUT" > "$LOGS"@" $RUN
#    cat $RUN
    sbatch $RUN
  done
done
