#pragma once

#include "cluster.hpp"

#include <iostream>
#include <random>
#include <bitset>


typedef std::bitset<SIZE> state;


class Hstring
{
  public:
  // constructor from physical parameters
  Hstring (double beta, double gamma, double J2);
  // constructor from vertices (test & debug)
  Hstring (double beta, double gamma, double J2, state alpha_, std::vector<vertex> vertices);
  // constructor from saved file
  Hstring (double beta, double gamma, double J2, std::size_t M, FILE *fin);
  // destructor
  ~Hstring();


  void save (FILE* fout) const;                 // save state in extern file
//  double get_beta() const { return P_diag*P_1st_nei/4.0/SIZE; }
//  double get_gamma() const { return 4.0*(1.0-P_2nd_nei)/P_1st_nei; }
//  double get_J2() const { return P_2nd_nei/P_1st_nei - 1.0; }
  std::size_t get_n() const { return n; }
  std::size_t get_M() const { return M; }
  state get_alpha() const { return alpha; }
	vertex operator[] (std::size_t p) const { return h_values[p]; }      // access element p
  friend std::ostream & operator<<(std::ostream & out, Hstring const & hstr);    // print the string


// updates
  bool rm_diag (std::size_t p);                // try to remove a diagonal operator at step p
  bool ins_diag (std::size_t p);               // choose between inserting plaquette operator or a spin unflip
  bool ins_plaq (std::size_t p, unsigned k);   // try to insert a given plaquette operator at step p (same function for 1st and 2nd neighb)
  bool ins_unflip (std::size_t p);             // try to insert a spin unflip operator at step p

  bool flip_cluster (std::vector<std::size_t> const& vertices);     // flip a whole cluster
  void diag_updates();                                         // perform a whole cycle of diagonal updates
  void off_diag_updates();                                     // try to flip every clusters
  std::size_t resize();                                        // adjust M to ensure n never reaches M
  void thermalize (unsigned q);                                // performs q sweeps and resize
  void sweep();                                                // a sweep includes one diagonal updates cycle and one off-diagonal updates cycle

  private:
  std::size_t n;        // number of hamiltonians in the string
  std::size_t M;        // length of the string
  double P_diag, P_1st_nei, P_2nd_nei;  // precompute some probabilities
  state alpha;     // state of the system: spin i is up if bit i is 1
  std::vector<vertex> h_values;
/* values a,b of the hamiltonians in the string. Convention:
h_values[p] = (0,0) --> element p is identity (no hamiltonian)
h_values[p] = (1,k) --> element p is the diagonal plaquette clus.list[k]: H = 3/2 - (sigma_A*sigma_B + sigma_B*sigma_C + sigma_C*sigma_1)
h_values[p] = (2,i+1) --> element p is a "spin unflip" at site i: H = Gamma
h_values[p] = (2,-i-1) --> element p is a spin flip at site i: H = Gamma * sigma^x_i
*/

  Cluster clus;          // allows to perform cluster updates
  std::mt19937 gen;      // pseudo-random number generator
  std::uniform_real_distribution<double> dis = std::uniform_real_distribution<double> (0.0,1.0);                  // get random probability
  std::uniform_int_distribution<unsigned> dis_site = std::uniform_int_distribution<unsigned> (1,SIZE);            // get random site
  std::uniform_int_distribution<unsigned> dis_plaq = std::uniform_int_distribution<unsigned> (0,2*SIZE-1);        // get random plaquette
  std::uniform_int_distribution<unsigned> dis_conf = std::uniform_int_distribution<unsigned> (0,80);              // get random config
};
