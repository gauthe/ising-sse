#include "binning.hpp"
#include <math.h>      // sqrt
#include <algorithm>   // std::max_element, std::max
#include <assert.h>


// constructor: need a minimum value for n_bins to perform tests with small N_meas_max. N_meas_max must be < 2^37
ScalarBinning::ScalarBinning (std::size_t N_meas_max): n_bins(std::max(unsigned(4),unsigned(log2(N_meas_max)) - 5)), filling_bin(n_bins,0), square_sum(n_bins,0) {}

ScalarBinning::~ScalarBinning()
{}

// add one value to current data
void ScalarBinning::add_value (double value)
{
  average += value;
  N_meas += 1;
  square_sum[0] += value*value;
  for (unsigned l=1; l<n_bins; ++l)
  {
    filling_bin[l] += value;               // add value to each bin
    if (!(N_meas%(1<<l)))                  // if the bin is full
    {
      square_sum[l] += filling_bin[l]/(1<<l)*filling_bin[l]/(1<<l);      // add the value to E(X^2)
      filling_bin[l] = 0.0;                                              // empty the bin
    }
  }
}


// get the errors for each order
std::vector<double> ScalarBinning::get_binning_errors(unsigned n_bins_curr) const
{
  std::vector<double> errors(n_bins_curr);
  std::size_t M_l(N_meas), M_corr;                   // number of values in bin l
  for (unsigned l=0; l<n_bins_curr; ++l)
  {
    M_corr = N_meas-N_meas%(1<<l);            // correction of the expected value due to unfilled bin
    errors[l] = sqrt( (square_sum[l]/M_l - (average-filling_bin[l])/M_corr*(average-filling_bin[l])/M_corr)/(M_l-1) );    // Var(X) = E(X^2) - E(X)^2
    M_l /= 2; 
  }
  return errors;
}

double ScalarBinning::get_autocorr_time() const
{
  unsigned n_bins_curr(unsigned(log2(N_meas))-5);    // current number of bins to consider
  std::vector<double> errors(get_binning_errors(n_bins_curr));
  return (errors[n_bins_curr-1]*errors[n_bins_curr-1]/errors[0]/errors[0] - 1)/2;
}


// get the final error margin
double ScalarBinning::get_error() const
{
  if (N_meas<256) { return 0; }   // no error without enough points
  unsigned n_bins_curr(unsigned(log2(N_meas))-5);    // current number of bins to consider
  std::vector<double> errors(get_binning_errors(n_bins_curr));

  double max_err(*std::max_element(errors.begin(),errors.end())); // if last element is not maximum, convergence occured and we return the largest error
  if (errors[n_bins_curr-1] < max_err) { return max_err; }
  else
  {
    double x1((1.0/(n_bins_curr-1) + 1.0/(n_bins_curr-2))/2), x2((1.0/(n_bins_curr-3) + 1.0/(n_bins_curr-2))/2);
    return (x1*(errors[n_bins_curr-2] + errors[n_bins_curr-3])/2-x2*(errors[n_bins_curr-1] + errors[n_bins_curr-2])/2)/(x1-x2);    // else, make a linear regression to extrapolate error
  }
}




// methods of class VectorBinning. Same methods, but for vectors (std::valarray are used as implementation)

// constructor: need a minimum value for n_bins to perform tests with small N_meas_max.  N_meas_max must be < 2^37.
VectorBinning::VectorBinning (unsigned long N_meas_max, unsigned vec_size_): n_bins(std::max(4,int(log2(N_meas_max)) - 5)), vec_size(vec_size_), average(vec_size_), filling_bin(n_bins,std::valarray<double>(vec_size)), square_sum(n_bins,std::valarray<double>(vec_size)) {}

VectorBinning::~VectorBinning()
{}

VectorBinning::VectorBinning (FILE *fin)
{
  fread(&n_bins,sizeof(unsigned),1,fin);
  fread(&vec_size,sizeof(unsigned),1,fin);
  fread(&N_meas,sizeof(unsigned long),1,fin);
  average = std::valarray<double> (vec_size);
  filling_bin = std::vector<std::valarray<double> > (n_bins,std::valarray<double>(vec_size));
  square_sum = std::vector<std::valarray<double> > (n_bins,std::valarray<double>(vec_size));

  for(double& av:average) { fread(&av,sizeof(double),1,fin); }
  for(std::size_t i=0; i<n_bins; i++)
  {
    for(double& fb:filling_bin[i]) { fread(&fb,sizeof(double),1,fin); }
  }
  for(std::size_t i=0; i<n_bins; i++)
  {
    for(double& sqs:square_sum[i]) { fread(&sqs,sizeof(double),1,fin); }
  }
}

void VectorBinning::save(FILE *fout) const
{
  fwrite(&n_bins,sizeof(unsigned),1,fout);
  fwrite(&vec_size,sizeof(unsigned),1,fout);
  fwrite(&N_meas,sizeof(unsigned long),1,fout);
  for(const double& av:average) { fwrite(&av,sizeof(double),1,fout); }
  for(std::size_t i=0; i<n_bins; i++)
  {
    for(const double& fb:filling_bin[i]) { fwrite(&fb,sizeof(double),1,fout); }
  }
  // optimize cache: use two separate loops
  for(std::size_t i=0; i<n_bins; i++)
  {
    for(const double& sqs:square_sum[i]) { fwrite(&sqs,sizeof(double),1,fout); }
  }
}

// add one value to current data
void VectorBinning::add_value (std::valarray<double> value)
{
  assert (value.size() == vec_size);       // check value has correct size
  average += value;
  N_meas += 1;
  square_sum[0] += value*value;
  for (unsigned l=1; l<n_bins; ++l)
  {
    filling_bin[l] += value;               // add value to each bin
    if (!(N_meas%(1<<l)))                  // if the bin is full
    {
      square_sum[l] += filling_bin[l]*filling_bin[l]/double(1<<l)/double(1<<l);      // add the value to E(X^2)
      filling_bin[l].resize(vec_size);                                       // empty the bin
    }
  }
}


// get the errors for each order
std::vector<std::valarray<double> > VectorBinning::get_binning_vector(unsigned n_bins_curr) const
{
  std::vector<std::valarray<double> > errors(n_bins_curr,std::valarray<double>(vec_size));
  std::size_t M_l(N_meas), M_corr;            // number of values in bin l
  for (unsigned l=0; l<n_bins_curr; ++l)
  {
    M_corr = N_meas-N_meas%(1<<l);            // correction of the expected value due to unfilled bin
    errors[l] = sqrt( (square_sum[l]*1.0/M_l - (average-filling_bin[l])/M_corr*(average-filling_bin[l])/M_corr)/(M_l-1) );    // Var(X) = E(X^2) - E(X)^2
    M_l /= 2; 
  }
  return errors;
}

// get the errors for each order only for index k
std::vector<double> VectorBinning::get_binning_vector (unsigned n_bins_curr, std::size_t k) const
{
  assert (k<vec_size);
  std::vector<double> errors(n_bins_curr);
  std::size_t M_l(N_meas), M_corr;               // number of values in bin l
  for (unsigned l=0; l<n_bins_curr; ++l)
  { 
    M_corr = N_meas-N_meas%(1<<l);               // correction of the expected value due to unfilled bin
    errors[l] = sqrt( (square_sum[l][k]*1.0/M_l - (average[k]-filling_bin[l][k])/M_corr*(average[k]-filling_bin[l][k])/M_corr)/(M_l-1) );    // Var(X) = E(X^2) - E(X)^2
    M_l /= 2;
  }
  return errors;
}


std::valarray<double> VectorBinning::get_autocorr_time() const
{
  if (N_meas<256) { return std::valarray<double> (vec_size); }   // no autcorrelation without enough points
  unsigned n_bins_curr(unsigned(log2(N_meas))-5);    // current number of bins to consider
  std::vector<std::valarray<double> > errors(get_binning_vector(n_bins_curr));
  return (errors[n_bins_curr-1]*errors[n_bins_curr-1]/errors[0]/errors[0] - 1)/2;
}

// same as previous one but returns a std::vector
std::vector<double> VectorBinning::vector_get_autocorr_time() const
{
  std::vector<double> vec;
  std::valarray<double> data(get_autocorr_time());
  vec.assign(std::begin(data), std::end(data));
  return vec;
}


// get the final error margin
std::valarray<double> VectorBinning::get_error() const
{
  if (N_meas<256) { return std::valarray<double>(vec_size); }   // no error without enough points
  unsigned n_bins_curr(unsigned(log2(N_meas))-5);    // current number of bins to consider (the larger ones do not contain enough points)
  std::vector<std::valarray<double> > binning_vector(get_binning_vector(n_bins_curr));
  std::valarray<double> error(vec_size);
  double x1((1.0/(n_bins_curr-1) + 1.0/(n_bins_curr-2))/2), x2((1.0/(n_bins_curr-3) + 1.0/(n_bins_curr-2))/2), max_err;
  for (std::size_t j=0; j<vec_size; ++j)
  { 
    max_err = binning_vector[0][j];
    for (unsigned i=0; i<n_bins_curr; ++i) { if (max_err < binning_vector[i][j]) { max_err = binning_vector[i][j]; } }
    if (binning_vector[n_bins_curr-1][j] < max_err) { error[j] = max_err; }   // if last element is not maximum, convergence occured and we return the largest error
    else
    {
      error[j] = (x1*(binning_vector[n_bins_curr-2][j] + binning_vector[n_bins_curr-3][j])/2-x2*(binning_vector[n_bins_curr-1][j] + binning_vector[n_bins_curr-2][j])/2)/(x1-x2);    // else, make a linear regression to extrapolate error
    }
  }
  return error;
}


// same as previous one but returns a std::vector
std::vector<double> VectorBinning::vector_get_error() const
{
  std::vector<double> vec;
  std::valarray<double> data(get_error());
  vec.assign(std::begin(data), std::end(data));
  return vec;
}


// return error for index k
double VectorBinning::get_error(std::size_t k) const
{
  assert (k<vec_size);
  if (N_meas<256) { return 0; }   // no error without enough points
  unsigned n_bins_curr(unsigned(log2(N_meas))-5);    // current number of bins to consider
  std::vector<double> errors_k(get_binning_vector(n_bins_curr,k));
  double max_err(*std::max_element(errors_k.begin(),errors_k.end())); // if last element is not maximum, convergence occured and we return the largest error
  if (errors_k[n_bins_curr-1] < max_err) { return max_err; }
  else
  { 
    double x1((1.0/(n_bins_curr-1) + 1.0/(n_bins_curr-2))/2), x2((1.0/(n_bins_curr-3) + 1.0/(n_bins_curr-2))/2);
    return (x1*(errors_k[n_bins_curr-2] + errors_k[n_bins_curr-3])/2-x2*(errors_k[n_bins_curr-1] + errors_k[n_bins_curr-2])/2)/(x1-x2);    // else, make a linear regression to extrapolate error
  }
}


// return std::vector of average values
std::vector<double> VectorBinning::vector_get_value() const
{
  std::vector<double> vec;
  std::valarray<double> data(get_value());
  vec.assign(std::begin(data), std::end(data));
  return vec;
}

