#include <fstream>       // std::getline
#include <sstream>       // std::sstream
#include "hstring.hpp"
#include "measurement.hpp"



// store computed data in extern file
void save_results (std::string const& output_file, std::string const& params, std::vector<double> const& values, std::vector<double> const& errors, std::size_t N_meas)
{ 
  std::ofstream data_stream;
  data_stream.open(output_file, std::ios::out | std::ios::trunc);
  if(!data_stream) { std::cout << "ERROR : unable to open file " << output_file << std::endl; }

// first lines contain parameters
  data_stream << params << N_meas << std::endl;
// 3rd line contains obersables values
  data_stream << values[SIZE]<<";"<<values[SIZE+1]<<";"<<values[SIZE+2]<<";"<<values[SIZE+3] <<";"<<values[SIZE+4] <<";"<<values[SIZE+5] <<";"<<values[SIZE+6] <<";"<< values[SIZE+7] << ";" <<values[SIZE+8] << std::endl;

// 4th line contains correlation functions
  for (unsigned i=0; i<SIZE-1; ++i) { data_stream << values[i] << ";"; }     // store values of correlation functions
  data_stream << values[SIZE-1] << std::endl;                                // avoid ; as last char

// 5th line contains error of observables
  data_stream << errors[SIZE]<<";"<<errors[SIZE+1]<<";"<<errors[SIZE+2]<<";"<<errors[SIZE+3] <<";"<<errors[SIZE+4] <<";"<<errors[SIZE+5] <<";"<< errors[SIZE+6] << ";" << errors[SIZE+7] << ";" << errors[SIZE+8] << std::endl;

// 6th line contains correlation functions errors
  for (unsigned i=0; i<SIZE-1; ++i) { data_stream << errors[i] << ";"; }     // store errors of correlation functions
  data_stream << errors[SIZE-1] << std::endl;                                // avoid ; as last char

  data_stream.close();
} 



int main (int argc, char *argv[])
{
  double param_phys[3];
  unsigned param_sim[5];

  try
  {
    if (SIZE != ROW*COL) { throw "ERROR: SIZE is not ROW*COL."; }
    if (ROW%6 | COL%6) { throw "ERROR: ROW and COL must be divisible by 6"; }
    if (argc<2) { throw "ERROR: please enter input file."; }
  }
  catch (const char* msg) { std::cerr << msg << std::endl; return -1; }

  FILE* fin = NULL;
  fin = fopen(argv[1],"rb");
  if (fin == NULL) { std::cerr << "ERROR: unable to open file"<<std::endl; return -1; }
  fread(param_phys,sizeof(double),3,fin);
  fread(param_sim,sizeof(unsigned),5,fin);
  double beta(param_phys[0]), gamma(param_phys[1]), J2(param_phys[2]);
  unsigned M(param_sim[0]), thermalization(param_sim[1]), N_meas_max(param_sim[2]), T_meas(param_sim[3]);
  if (SIZE != param_sim[4]) { std::cerr << "ERROR: SIZE do not match in save file and exe."; return -1; }
  Hstring hstr(beta, gamma, J2, M, fin);
  Measurement meas(hstr, beta, gamma, J2, fin);
  fclose(fin);


  unsigned N_meas_start(meas.get_N_meas());
  long start(time(0));

  std::cout<<"Computation relaunched. Data loaded from file."<<std::endl;
  std::cout<<"Geometry: HEXA, SIZE = "<<SIZE<<", ROW = " << ROW << ", COL = "<<COL<<std::endl;
#ifdef FORCE_STRIPE
  std::cout << "FORCE_STRIPE activated" << std::endl;
#endif
  std::cout<<"Physical parameters: beta = "<<beta<<", gamma = "<<gamma<< ", J2 = " << J2 << std::endl;
  std::cout<<"Simulation parameters: thermalization = "<<thermalization<<", T_meas = "<<T_meas <<", N_meas_max = "<<N_meas_max<<std::endl;
  std::cout<<"N_meas = "<< N_meas_start << "; current state of Hstring:\n" << hstr << std::endl;
  std::cout << "E = " << -double(hstr.get_n()) / SIZE / beta + 3*(1+J2) + gamma << std::endl;

  state alpha(hstr.get_alpha());
  for (int i=ROW-1; i>-1; i--)
  {
    for (int j=0; j<i; j++) { std::cout << " "; }
    for (int j=0; j<COL; j++) { std::cout << (alpha[i*COL+j] ? "o" : "x") << " "; }
    std::cout << std::endl;
  }
  std::stringstream s;
  s.precision(2);
// construct output file name. Ensure that *beta1.1-gamma* > *beta1-gamma* to read files with increasing beta / gamma / J2
#ifdef FORCE_STRIPE
  s << "data_L" << COL << "_FS_beta" << std::fixed << beta << "gamma" <<  gamma << "J2_" <<  J2<<".txt";
#else
  s << "data_L" << COL << "beta" << std::fixed << beta << "gamma" <<  gamma << "J2_" <<  J2<<".txt";
#endif
  std::string output(s.str());

  s.str("");  // empty s to construct string of fixed parameters
// 1st line contains geometry, 2nd line contains parameters
  s << SIZE << ";" << ROW << ";" << COL << std::endl << beta << ";" << gamma << ";"<<J2 << ";"<<thermalization<< ";"<<T_meas<<";";
  std::string params(s.str());

  FILE *fout = NULL;

// core of the program
  for (unsigned N_meas=N_meas_start; N_meas<N_meas_max; N_meas++) {
    for (unsigned sweep=0; sweep<T_meas; sweep++) {
      hstr.sweep();
    }
    meas.measure(hstr);
    if (!(N_meas%98304))       // save data every 100k measurements. Use power of 2 for binning.
    {
      save_results(output, params, meas.get_values(), meas.get_errors(), N_meas);
      fout = fopen(argv[1],"wb");
      fwrite(param_phys, sizeof(double),3,fout);
      fwrite(param_sim, sizeof(unsigned),5,fout);
      hstr.save(fout);
      meas.save(fout);
      fclose(fout);
      std::cout<< "N_meas = "<<N_meas<<"; time = "<< time(0) - start<<"; data saved"<<std::endl;
    }
  }

  assert (N_meas_max == meas.get_N_meas());
  std::cout<< "N_meas = "<< meas.get_N_meas() << "; measurements finished"<<std::endl;

// get results
  std::vector<double> values = meas.get_values();
  std::vector<double> errors = meas.get_errors();
  save_results(output, params, values, errors, meas.get_N_meas());
  for (unsigned i=0; i<SIZE; ++i) { std::cout << "C(0," << i << ") = " << values[i] << " +/- "<< errors[i] <<"\n"; }
  std::cout << "m = " << values[SIZE+1] <<" +/- "<< errors[SIZE+1] << std::endl;
  std::cout << "chi_m = " << values[SIZE+3] <<" +/- "<< errors[SIZE+3] << std::endl;
  std::cout << "stripe order parameter = " << values[SIZE+5] <<" +/- "<< errors[SIZE+5] << std::endl;
  std::cout << "stripe order susceptibility = " << values[SIZE+6] <<" +/- "<< errors[SIZE+6] << std::endl;
  std::cout << "U = " << values[SIZE+7] <<" +/- "<< errors[SIZE+7] << std::endl;
  std::cout << "FS flip = " << values[SIZE+2] << " +/- "<< errors[SIZE+2] << std::endl;
  std::cout << "FS J2 = " << values[SIZE+8] << " +/- "<< errors[SIZE+8] << std::endl;
  std::cout << "thermal capacity = " << values[SIZE+4] << " +/- " << errors[SIZE+4] << std::endl;
  std::cout << "energy = " << values[SIZE] << " +/- " << errors[SIZE] << std::endl;

  std::cout << "data has been written in file " << output << std::endl;
  std::cout << "time = " << time(0) - start << " s" << std::endl;

  return 0;
}
