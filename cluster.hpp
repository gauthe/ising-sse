#pragma once

#include <vector>
#include <string>
#include <assert.h>


class Hstring;
typedef std::pair<int, int> vertex;
typedef unsigned plaquette[3];

class Cluster
{
  friend class Hstring;

  public:
  // constructor from initial size
  Cluster (std::size_t M0, Hstring const& hstr_);

  void fill_link (unsigned i, std::size_t q);
  void fill_link (std::size_t q0, vertex v);
  void rejoin();
  void resize (std::size_t M) { link.resize(6*M,-1); min_pri.resize(M); }
  void explore (std::size_t q, std::vector<std::size_t> &vertices);
  void reset();
	bool is_empty() const;
  void set_privileges (unsigned config);   // set config for privileged legs

  private:
  std::vector<long> link;   // linked list: link vertices following imaginary time
  std::vector<bool> min_pri;// if hstr[p] is a plaquette, min_pri[p] = privileged site == minority site
  long first[SIZE];         // first time site i interacts. Must always be even
  long last[SIZE];          // last time site i interacts. Must always be odd
  plaquette plaq_list[4*SIZE];   // list of plaquettes
  Hstring const& hstr;      // ref to hstr to get vertex type
  int privileges[6];     // privileges for the lattice and the 3 sublattices: for plaquette number k, privileges[3*k/2/N] gives privileges
};

