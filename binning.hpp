/*
implements binning analysis to compute the error bars of a Monte Carlo simulation with non-zero autocorrelation time.
ref: DOI: 10.1119/1.3247985

Designed for large number of sampes, return 0 for less than 256.
*/

#pragma once

#include <vector>
#include <valarray>

class ScalarBinning
{
  public:
  // constructor
  ScalarBinning (std::size_t N_meas_max);       // N_meas_max fixes the number of bin
  ~ScalarBinning();

  unsigned get_n_bins() const { return n_bins; }
  std::size_t get_N_meas() const { return N_meas; }
  double get_value() const { return average/N_meas; }

  void add_value (double value);
  std::vector<double> get_binning_errors(unsigned n_bins_curr) const;       // return errors estimation for each relevant order of binning analysis (ie enough data at this order)
  double get_autocorr_time() const;                     // get the autocorrelation time
  double get_error() const;                             // return reliable estimation of the error for the observable

  private:
  unsigned n_bins;
  std::size_t N_meas = 0;
  double average = 0;
  std::vector<double> filling_bin;      // vector of bins; when a bin is filled its value is reset to 0
  std::vector<double> square_sum;       // sum of squared bins values for each order
};


class VectorBinning
{
  public:
  // constructor
  VectorBinning (unsigned long N_meas_max, unsigned vec_size_);      // N_meas_max fixes the number of bins
  VectorBinning (FILE *fin);  // constructor from save 
  ~VectorBinning();
  void save (FILE *fout) const;  // save current state in extern file

  unsigned get_n_bins() const { return n_bins; }
  unsigned get_vec_size() const { return vec_size; }
  unsigned long get_N_meas() const { return N_meas; }
  std::valarray<double> get_value() const { return average/double(N_meas); }
  double get_value(std::size_t k) const { return average[k]/N_meas; }

  void add_value (std::valarray<double> value);
  std::vector<std::valarray<double> > get_binning_vector(unsigned n_bins_curr) const;         // return errors estimation for each relevant order of binning analysis (ie enough data at this order)
  std::vector<double> get_binning_vector (unsigned n_bins_curr, std::size_t k) const;         // the same for index k only
  std::valarray<double> get_autocorr_time() const;                      // get the autocorrelation time
  std::valarray<double> get_error() const;                              // return reliable estimation of the error for the observable
  double get_error(std::size_t k) const;                                // the same for index k only

// same fonctions but return a std::vector
  std::vector<double> vector_get_autocorr_time() const;
  std::vector<double> vector_get_value() const;
  std::vector<double> vector_get_error() const;

  private:
  unsigned n_bins, vec_size;
  unsigned long N_meas = 0;
  std::valarray<double> average;
  std::vector<std::valarray<double> > filling_bin;      // vector of bins; when a bin is filled its value is reset to 0
  std::vector<std::valarray<double> > square_sum;       // sum of squared bins values for each order
};

