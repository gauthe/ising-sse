#include "measurement.hpp"
#include "hstring.hpp"


// constructor
Measurement::Measurement (Hstring const& hstr, unsigned long N_meas_max, double beta_, double gamma_, double J2_): bins(N_meas_max,SIZE+11), M(hstr.get_M()), gen_meas(time(0)), binom_dis(M,0.5), beta(beta_), gamma(gamma_), J2(J2_)
{
  for (unsigned i=0; i<SIZE; ++i)
  {
/*
m is the order parameter of the bond_ordered phase. It is defined as sqrt( (m1-m2)^2 + (m2-m3)^2 + (m3-m4)^2), where m1-2-3 are the magnetization on the three sublattices.
up to a sqrt(3/2) factor added at the end, m = abs( m1 + m2*exp(2i pi/3) + m3*exp(-2i pi/3)/sqrt(3) )

stripe_order is the order parameter for the stripe-ordered phase. It is defined as sqrt(stripe0^2 + stripe1^2 + stripe2^2), stripe_order0 is 1 for even colons and -1 for odd colons, stripe_order1 is the same for rows and stripe_order2 is the same for (row+col)
min value is 0, max value is 1

*/
    switch  ((i/COL+2*(i%COL))%3)
    {
      case 0:
        sub_latt1[i] = true;
        break;
      case 1:
        sub_latt2[i] = true;
        break;
      case 2:
        sub_latt3[i] = true;
        break;
    }
    if ((i%COL)%2) { stripe_geom0[i] = true; }
    if ((i/COL)%2) { stripe_geom1[i] = true; }
    if ((i%COL+i/COL)%2) { stripe_geom2[i] = true; }
  }
}


Measurement::Measurement (Hstring const& hstr, double beta_, double gamma_, double J2_, FILE *fin): bins(fin), M(hstr.get_M()), binom_dis(M,0.5), beta(beta_), gamma(gamma_), J2(J2_)
{
  fread(&gen_meas,sizeof(std::mt19937),1,fin);
  for (unsigned i=0; i<SIZE; ++i)
  {
    switch  ((i/COL+2*(i%COL))%3)
    {
      case 0:
        sub_latt1[i] = true;
        break;
      case 1:
        sub_latt2[i] = true;
        break;
      case 2:
        sub_latt3[i] = true;
        break;
    }
    if ((i%COL)%2) { stripe_geom0[i] = true; }
    if ((i/COL)%2) { stripe_geom1[i] = true; }
    if ((i%COL+i/COL)%2) { stripe_geom2[i] = true; }
  }
}


Measurement::~Measurement()
{}

void Measurement::save (FILE *fout) const
{
  bins.save(fout);
  fwrite(&gen_meas,sizeof(std::mt19937),1,fout);
}


void Measurement::measure(Hstring const& hstr)
{
  assert (M == hstr.get_M());                     // M should not change after thermalization

  std::size_t n(hstr.get_n()), l(binom_dis(gen_meas));
  state alpha(hstr.get_alpha());
  unsigned flipL(0), flipR(0), J2_L(0), J2_R(0);
  std::valarray<double> temp(SIZE+11), value(SIZE+11);

// compute correlation functions
  for (unsigned i=0; i<SIZE; ++i)
  {
    temp[i] = 1 - 2*(alpha[0]^alpha[i]);     // <C(i)> = <S(0)S(i)>, assuming invariance translation
  }

// compute order parameters
  int m1 = (sub_latt1&alpha).count();     // magnetization of the 1st sublattice (actually, mag1 = 2*m1 - SIZE/3)
  int m2 = (sub_latt2&alpha).count();     // magnetization of the 2nd sublattice
  int m3 = (sub_latt3&alpha).count();     // magnetization of the 3rd sublattice
// bond order parameter is sqrt of the staggered magnetization on 2 sublattices. SIZE+3 cancels with substraction, factor 2 is added later
  temp[SIZE+1] = sqrt( (m1-m2)*(m1-m2) + (m2-m3)*(m2-m3) + (m3-m1)*(m3-m1) );


  int stripe_order0 = (alpha^stripe_geom0).count();
  int stripe_order1 = (alpha^stripe_geom1).count();
  int stripe_order2 = (alpha^stripe_geom2).count();
  temp[SIZE+5] = sqrt( (2*stripe_order0-SIZE)*(2*stripe_order0-SIZE) + (2*stripe_order1-SIZE)*(2*stripe_order1-SIZE) + (2*stripe_order2-SIZE)*(2*stripe_order2-SIZE) );

  unsigned n_diag(0);                          // number of diag operators between 2 off-diag operators
  for (std::size_t p=0; p<M; ++p)              // for all steps p
  {
    if (hstr[p].first)                         // if there is a vertex at step p
    {
      if (hstr[p].second < 0)                  // if the operator is off-diagonal
      {
        assert (hstr[p].first == 2);
        value += double(n_diag)*temp;          // most operators are diagonal: add to value only at off-diag operators
        n_diag = 0;
        unsigned i = -hstr[p].second - 1;      // site whose spin changes
        alpha.flip(i);                         // if the vertex is a spin flip, propagate state alpha
        p < l ? ++flipL : ++flipR;             // take the vertex into account for FS computation
        temp[i] *= -1;
        if (!i) { temp *= -1; }                // 0 is a special case, since C(i) = S(0)S(i); temp[SIZE --> SIZE+7] will be reinitialized
        m1 += (2*alpha[i]-1)*sub_latt1[i];
        m2 += (2*alpha[i]-1)*sub_latt2[i];
        m3 += (2*alpha[i]-1)*sub_latt3[i];
        temp[SIZE+1] = sqrt( (m1-m2)*(m1-m2) + (m2-m3)*(m2-m3) + (m3-m1)*(m3-m1) );

        stripe_order0 += 2*(alpha[i]^stripe_geom0[i]) - 1;
        stripe_order1 += 2*(alpha[i]^stripe_geom1[i]) - 1;
        stripe_order2 += 2*(alpha[i]^stripe_geom2[i]) - 1;
        temp[SIZE+5] = sqrt( (2*stripe_order0-SIZE)*(2*stripe_order0-SIZE) + (2*stripe_order1-SIZE)*(2*stripe_order1-SIZE) + (2*stripe_order2-SIZE)*(2*stripe_order2-SIZE) );
      }
      else { if (hstr[p].first == 1 && hstr[p].second > 2*SIZE-1) { p < l ? J2_L++ : J2_R++; } }    // FS for J2
      n_diag += 1;                             // count the number of diagonal operators before next off-diagonal
    }
  }
  value += double(n_diag)*temp;
  assert (alpha == hstr.get_alpha());          // check periodicity in imaginary time
// we suppose that beta is large enough such that n is always non zero
  value /= double(n);
  value[SIZE] = n;
  value[SIZE+2] = flipL*flipR;                       // FS for spin flip
  value[SIZE+3] = value[SIZE+1]*value[SIZE+1];       // bond_order^2 / stmag^2 for chi
  value[SIZE+4] = n*n;                               // n^2 for capacity
  value[SIZE+6] = value[SIZE+5]*value[SIZE+5];       // stripe_order^2 for chi
  value[SIZE+7] = value[SIZE+3]*value[SIZE+3];       // m^4 for Binder cumulant
  value[SIZE+8] = J2_L*J2_R;                         // FS for J2
  value[SIZE+9] = flipL;                             // flipL for FS computation; <flipL> = <flipR>
  value[SIZE+10] = J2_L;                             // J2_L for FS computation

  bins.add_value(value);
}


std::vector<double> Measurement::get_values() const       // get results of measurements
{
  std::vector<double> values(bins.vector_get_value());

  values[SIZE+7] = 1.0 - values[SIZE+7]/3.0/values[SIZE+3]/values[SIZE+3];               // (scale to 0) U = 1 - <m^4>/3/<m^2>^2
  values[SIZE+3] = beta*6.0*(values[SIZE+3] - values[SIZE+1]*values[SIZE+1]);            // (extensive) m susceptiblity
  values[SIZE+1] *=sqrt(6.0)/SIZE;             // (intensive) m, with constant sqrt(3/2) to follow Isakov and Moessner
  values[SIZE+2] = (values[SIZE+2] - values[SIZE+9]*values[SIZE+9])/2/gamma/gamma;       // FS for spin flip (extensive)
  values[SIZE+6] = beta*(values[SIZE+6] - values[SIZE+5]*values[SIZE+5]);        // (extensive) stripe susceptiblity
  values[SIZE+5] /= SIZE;                      // (intensive) stripe order parameter = sqrt(stripe1^2 + stripe2^2 + stripe3^2)
  values[SIZE+4] = (values[SIZE+4] - values[SIZE]*(values[SIZE] + 1))/SIZE;      // (intensive) per site thermal capacity
  values[SIZE+8] = (values[SIZE+8] - values[SIZE+10]*values[SIZE+10])/2/J2/J2;   // FS for J2 (extensive)
  values[SIZE] = -values[SIZE] / SIZE / beta + 3*(1+J2) + gamma;                 // (intensive) per site energy: E = epsilon - <n>/beta
  values.pop_back();    // remove flipL (no physical meaning alone)
  values.pop_back();    // remove J2_L (no physical meaning alone)
  return values;
}


std::vector<double> Measurement::get_errors() const       // get margins of error of measurements
{ 
  std::vector<double> errors(bins.vector_get_error());
  double m2 = bins.get_value(SIZE+3);   // <m^2>

  errors[SIZE+7] = (errors[SIZE+7] + bins.get_value(SIZE+7)*2*errors[SIZE+3]/m2)/3.0/m2/m2;      // Binder Cumulant
  errors[SIZE+3] = beta*6.0*(errors[SIZE+3] + 2*bins.get_value(SIZE+1)*errors[SIZE+1]);          // bond susceptiblity
  errors[SIZE+1] *= sqrt(6.0)/SIZE;                                                              // m
  errors[SIZE+2] = (errors[SIZE+2] + 2*bins.get_value(SIZE+9)*errors[SIZE+9])/2/gamma/gamma;     // FS
  errors[SIZE+6] = beta*(errors[SIZE+6] + 2*bins.get_value(SIZE+5)*errors[SIZE+5]);              // stripe susceptiblity
  errors[SIZE+5] /= SIZE;                                                                        // stripe order parameter
  errors[SIZE+4] = (errors[SIZE+4] + (2*bins.get_value(SIZE)+1)*errors[SIZE]) / SIZE;            // per site thermal capacity
  errors[SIZE+8] = (errors[SIZE+8] + 2*bins.get_value(SIZE+10)*errors[SIZE+10])/2/J2/J2;         // FS
  errors[SIZE] /= SIZE*beta;               // energy
  errors.pop_back();                       // remove flipL
  errors.pop_back();                       // remove J2_L
  return errors;
}

