#include "cluster.hpp"
#include "hstring.hpp"


// constructor from initial size
Cluster::Cluster (std::size_t M0, Hstring const& hstr_): link(6*M0, -1), min_pri(6*M0), hstr(hstr_)
{
  std::fill_n(first, SIZE, -2);
  std::fill_n(last, SIZE, -1);

/*
fill the array of plaquette such that:
for k from 0 to 2*SIZE-1, plaq_list[k] is first neighbour plaquette
plaq_list[k][j] belongs to sulattice j => plaq_list[k][j]%COL + 2*(plaq_list[k][j]/COL) = j
for k from 2*SIZE to 2*SIZE+2*SIZE/3, plaq_list[k] is second neighbour plaquette, sublattice 0
for k from 2*SIZE+2*SIZE/3 to 2*SIZE+4*SIZE/3, plaq_list[k] is second neighbour plaquette, sublattice 1
for k from 2*SIZE+4*SIZE/3 to 4*SIZE, plaq_list[k] is second neighbour plaquette, sublattice 2
for each of the three sublattices, plaq_list[k][j] belongs to sub-sublattice j => plaq_list[k][j]%3 = j
at the end, 3*k/2/SIZE gives sublattice: 0-1-2 = initial lattice, 3 = sublattice 0, 4 = sublattice 1, 5 = sublattice 2
*/
  for (unsigned i=0; i<SIZE; ++i)
  {
    unsigned p = i/COL;
    unsigned r = i%COL;
    plaq_list[2*i][(2*p+r)%3] = i;
    plaq_list[2*i][(2*p+r+1)%3] = p*COL + (r+1)%COL;
    plaq_list[2*i][(2*(p+1)+r)%3] = ((p+1)%ROW)*COL + r;

    plaq_list[2*i+1][(2*p+r)%3] = i;
    plaq_list[2*i+1][(2*(p+1)+r)%3] = ((p+1)%ROW)*COL + r;
    plaq_list[2*i+1][(2*p+r+1)%3] = ((p+1)%ROW)*COL + (r+COL-1)%COL;

    plaq_list[(3+(2*p+r)%3)*2*SIZE/3+i%(SIZE/3)][i%3] = i;
    plaq_list[(3+(2*p+r)%3)*2*SIZE/3+i%(SIZE/3)][(r+2)%3] = ((p-1+ROW)%ROW)*COL + (r+2)%COL;
    plaq_list[(3+(2*p+r)%3)*2*SIZE/3+i%(SIZE/3)][(r+1)%3] = ((p+1)%ROW)*COL + (r+1)%COL;

    plaq_list[(7+2*((2*p+r)%3))*SIZE/3+i%(SIZE/3)][i%3] = i;
    plaq_list[(7+2*((2*p+r)%3))*SIZE/3+i%(SIZE/3)][(r+1)%3] = ((p+1)%ROW)*COL + (r+1)%COL;
    plaq_list[(7+2*((2*p+r)%3))*SIZE/3+i%(SIZE/3)][(r+2)%3] = ((p+2)%ROW)*COL + (r+COL-1)%COL;
  }
}


// fill the array link[] to make a linked list of sites belonging to one cluster
void Cluster::fill_link (unsigned i, std::size_t q)
{
  assert (!(q%2));
  if (last[i]<0) { first[i] = q; }       // if the child has no parent, ie no previous vertex on site i, initiate first[]
  else
  {                                      // if there has already been a vertex on site i
    link[last[i]] = q;                   // the parent gets a child
    link[q] = last[i];                   //  the child gets a parent
  }
  last[i] = q + 1;                       // remember last vertex on site i
}


// define how to fill array link[] depending on vertex type
void Cluster::fill_link(std::size_t q0, vertex v)
{
  assert (!(q0%6));
  assert (v.first);                          // check there is a vertex
  if (v.first == 2)                          // if the vertex is a spin opeator, ie has 2 legs
  {
    fill_link(abs(v.second)-1, q0);
  }
  else
  {
    assert (v.first == 1);
    fill_link(plaq_list[v.second][0],q0);    // pl[0] corresponds to legs 0 and 1
    fill_link(plaq_list[v.second][1],q0+2);
    fill_link(plaq_list[v.second][2],q0+4);
  }
}


// rejoin limits at p=M and p=0 to ensure periodicity
void Cluster::rejoin() 
{
  for (unsigned i=0;i<SIZE; ++i)
  {
    if(last[i] > -1) {              // if spin interacts at least once
      link[last[i]] = first[i];     // finish the linked list by giving child to last parent
      link[first[i]] = last[i];     // and parent to first child
    }
  }
}

void Cluster::set_privileges (unsigned config)
{
  assert (config < 81);
// use a size-6 array such that for plaquette number k, 3*k/2/SIZE gives privileges for the sublattice k belongs to
  privileges[0] = config%3;
  privileges[1] = config%3;
  privileges[2] = config%3;
  privileges[3] = (config/3)%3;
  privileges[4] = (config/9)%3;
  privileges[5] = config/27;
}


// explore the linked list following one cluster
void Cluster::explore(std::size_t q, std::vector<std::size_t> &vertices)
{
  assert (link[q] > -1);           // check this leg has not already been visited
	assert (hstr[q/6].first > 0);    // check there is a vertex at step p
	std::size_t q0 = q - q%6;        // get index of leg0 (q0 = 6*p)
	vertex v = hstr[q0/6];

	if (v.first == 1)                // if the vertex is a plaquette
	{
		assert(link[q+1-2*(q%2)] > -1);    // check exit leg has not already been visited
		if (min_pri[q0/6])             // if privieged site is minority, explore every legs
		{
			long leg0(link[q0]), leg1(link[q0+1]), leg2(link[q0+2]), leg3(link[q0+3]), leg4(link[q0+4]), leg5(link[q0+5]);
			link[q0] = -1;               // mark each leg as visited
			link[q0+1] = -1;
			link[q0+2] = -1;
			link[q0+3] = -1;
			link[q0+4] = -1;
			link[q0+5] = -1;
			if (link[leg0] > -1) { explore (leg0, vertices); }    // visit each leg if it has not already been visited
			if (link[leg1] > -1) { explore (leg1, vertices); }
			if (link[leg2] > -1) { explore (leg2, vertices); }
			if (link[leg3] > -1) { explore (leg3, vertices); }
			if (link[leg4] > -1) { explore (leg4, vertices); }
			if (link[leg5] > -1) { explore (leg5, vertices); }
		}
		else
		{
			if ((q%6)/2 == privileges[3*v.second/2/SIZE])      // else, if entry site is privileged, exit only by him
			{
				long leg0(link[q]), leg1(link[q+1-2*(q%2)]);
				link[q] = -1;                                    // mark entry and exit legs as visited
				link[q+1-2*(q%2)] = -1;
				if (link[leg0] > -1) { explore (leg0, vertices); }   // explore them if it has not been already done
				if (link[leg1] > -1) { explore (leg1, vertices); }
			}
			else                                        // if entry leg is not privileged, exit by every non-privileged legs
			{
				long leg2(link[q0+(2*privileges[3*v.second/2/SIZE]+2)%6]), leg3(link[q0+(2*privileges[3*v.second/2/SIZE]+3)%6]), leg4(link[q0+(2*privileges[3*v.second/2/SIZE]+4)%6]), leg5(link[q0+(2*privileges[3*v.second/2/SIZE]+5)%6]);

				link[q0+(2*privileges[3*v.second/2/SIZE]+2)%6] = -1;  // mark each leg as visited
				link[q0+(2*privileges[3*v.second/2/SIZE]+3)%6] = -1;
				link[q0+(2*privileges[3*v.second/2/SIZE]+4)%6] = -1;
				link[q0+(2*privileges[3*v.second/2/SIZE]+5)%6] = -1;

				if (link[leg2] > -1) { explore (leg2, vertices); }    // visit each leg if it has not already been visited
				if (link[leg3] > -1) { explore (leg3, vertices); }
				if (link[leg4] > -1) { explore (leg4, vertices); }
				if (link[leg5] > -1) { explore (leg5, vertices); }
			}
		}
	}
	else                          // if the vertex has only 2 legs, it is a spin (un)flip and cluster stops in the exit direction
	{
		assert (v.first == 2);      // check vertex is a spin operator
		vertices.push_back(q0/6);   // get index p of the vertex, which may be flipped
		if (link[link[q]] > -1)     // if entrance leg has not already been visited
		{
			assert(!(q%6));                      // check exploration starts with an entrance leg
			std::size_t leg0(link[q]);           // save value of next vertex for entrance leg
			link[q] = -1;                        // mark this leg as visited
			explore(leg0, vertices);             // explore entrance
		}
		else { link[q] = -1; }      // else, entrance leg has already been visited. Just mark it as visited
	}
  assert (link[q] < 0);         // check entrance leg has been marked as visited
}


// reset cluster for a new diagonal updates cycle
void Cluster::reset()
{
  assert(is_empty());              // check link[] has been completly explorated
  std::fill_n(last, SIZE, -1);     // reset last[] to build new cluster linked list link[]
}

bool Cluster::is_empty() const
{
  for (std::size_t q=0, M6=link.size(); q<M6; ++q)
  {
    if (link[q] > -1) { return false; }
  }
  return true;
}
