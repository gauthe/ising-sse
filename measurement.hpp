#pragma once

#include "hstring.hpp"
#include "binning.hpp"

class Measurement
{
  public:
  // Constructor from THERMALIZED hstring:
  Measurement (Hstring const& hstr, unsigned long N_meas_max, double beta_, double gamma_, double J2_);
  // constructor from saved data in extern file
  Measurement (Hstring const& hstr, double beta_, double gamma_, double J2_, FILE *fin);

  ~Measurement();

  // save data in extern file
  void save (FILE *fout) const;

  // mesasure observables on current hstring
  void measure (Hstring const& hstr);

  std::size_t get_N_meas() const { return bins.get_N_meas(); }
//  double get_beta() const { return beta; }
//  double get_gamma() const { return gamma; }
//  double get_J2() const { return J2; }
  std::vector<double> get_autocorr_time() const { return bins.vector_get_autocorr_time(); }
  std::vector<double> get_values() const;       // get results of the measurements. results[i] is the result of the observable obs[i]
  std::vector<double> get_errors() const;       // get errors of the measurements. errors[i] is the error of the observable obs[i]


  private:
/* binning analysis
For speed and simplicity, many observables are computed up to some constant, which is added only when data and error bars are stored
0 --> SIZE-1: corr_func
SIZE: n; per site energy E = (epsilon - <n>/beta)/SIZE returned as value (intensive)
SIZE+1: stmag / m  (intensive)
SIZE+2: flipL*flipR, FS = (<kL*kR> - <kL>*<kR>)/(2*gamma^2) returned as value (extensive)  --> spin flip left/right of l
SIZE+3: stmag^2 / m^2; chi = beta*(<phi^2> - <phi>^2) returned as value (scale to SIZE^4 / SIZE^2))
SIZE+4: n^2; per site thermal capacity C = (<n^2> - <n>^2 - <n>)/SIZE returned as value (intensive)
SIZE+5: stripe_order (intensive)
SIZE+6: stripe_order^2; chi = beta*(<phi^2> - <phi>^2) returned as value (scale as SIZE^2)
SIZE+7: m^4; Binder cumulant U = 1 - <m^4>/3/<m^2>^2 returned as value (scale as 0)
SIZE+8: J2_L*J2_R, FS = (<kL*kR> - <kL>*<kR>)/(2*J2^2) returned as value (extensive)   --> 2nd nei plaq left/right of l
SIZE+9: flipL; <kL> = <kR>; used for FS computation. Pop back, nothing returned
SIZE+10: J2_L; <kL> = <kR>; used for FS computation. Pop back, nothing returned
*/
  VectorBinning bins;

  std::size_t M;           // M MUST NOT change after thermalization
// use bitsets to compute staggered magnetization and order parameters; defauts constructor initializes it with 0
  state sub_latt1, sub_latt2, sub_latt3, stripe_geom0, stripe_geom1, stripe_geom2;

// FS
  std::mt19937 gen_meas;
  std::binomial_distribution<std::size_t> binom_dis;

  double beta, gamma, J2;         // inverse of temperature, transverse field, second neighbour interaction

};
